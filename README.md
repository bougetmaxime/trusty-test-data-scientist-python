# Trusty's Data Scientist Challenge: Python

First of all, **congrats** !  You've successfully made your way through the first steps of our recruitment process.

In this test, your will develop an algorith which can score a list of CV profiles base on the sentence: **Is this CV profile is a Data Scientist Profil or Not ?**.

## Guidelines

- Clone this repo (do **not** fork it)
- Solve the exercices in ascending order
- Only do one commit per exercice and include the `.git` when submiting your test

## Pointers

Trusty’s technical test is built to help you express yourself as a data scientist and give us an overview of your ability to code on Python stacks. This exercise gives you a lot of liberty in the structure's logic and your technical choices. Feel free to keep an opened mind and to suggest other features that seem relevant to you. 

Our Tech Team will pay great attention to:
- your files organization,
- your code quality,
- your parsimony logic:  **the less the better**
- your technical choices,
- your ability to propose : **thinking outside the box**

Please also note that:
- Running `$ python3 application.py` from the exercice folder should generate the desired output, but of course feel free to add more files if needed. Here we would evaluate your ability to code in Python in a native way *without using a Read–Eval–Print Loop (REPL) as Jupyter Notebook*
- ⚠️ In case of impossibility to complete the test without using REPL interface, you can use it but this will be take in count when evaluate your test. 

## Sending Your Results

Once you are done, please send your results to Trusty.

`maxime@hellotrusty.io`

You can send your Github/Gitlab/... project link or zip your directory and send it via email.
If you do not use Github/Gitlab/..., don't forget to attach your `.git` folder.

Good luck, and talk soon!
Trusteam

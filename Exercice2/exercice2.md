# Intro

In this exercice, we're going to ask you to develop an algorithm which can score a Cv Profile as **Data Sciencist**.

# Exercice 2

## Directions
- By using your **algorithm on exercice 1**, you need to develop a algorithm which can score every Cv Profile as **Data Scientist** 
- Test your algorithm on `list_profile_experience_to_score.json`
- Write a documentation to explain how work your algorithm.
- If you run out of times, please keep 10 minutes to describe what your next steps might be.

## Input 
You need to restart from the algorith you developed on **Exercice 1** and Score the Cv profiles list from `list_profile_experience_to_score.json` file.

## Output 
When run `$ python3 application.py`, we need to get your algorithm results with an expected format as show on `expected_output.json` JSON file.